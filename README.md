# Ethereum Workshop
Welcome to our Ethereum workshop. After completing this workshop you will be able to
interact with the Ethereum network and also deploy your own smart contracts.
Specifically you will learn to:
* own and transfer Ether
* interact with existing smart contracts
* deploy your own smart contract
* tokenize a real-world asset (i.e., chocolate)

## Your incentive
As with any blockchain project, there has to be an economic aspect to it, not just
a technical one. This workshop is no exception. We would like to reward you with
chocolate for completing your tasks. In order for you to trust that you will receive the
chocolate, we assigned a trusted chocolate master. The master is not a human, but a
smart contract. We divided all the chocolate we have in chocolate tokens and put it in
the custody of the chocolate master. Every time you accomplish a task the chocolate
master will check it and give you a chocolate token. At the end of the workshop you
will show us how many chocolate tokens you have in your wallet and we'll hand you
in the physical chocolate coins.

## Workshop structure
The workshop has two parts: practice and exercises. First you will set up everything
and learn to interact with smart contracts. Then you will have to do some exercises in
order to win chocolate tokens.

## Practice
### Setting up Metamask
In order to interact with Ethereum, we have to install an Ethereum client. We will use
a browser client called Metamask, that can be installed as a browser extension in
Firefox or Google Chrome. You can install Metamask from here:

* for Firefox: https://addons.mozilla.org/en-US/firefox/addon/ether-metamask/
* for Google Chrome: https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn

Now you should see it in your addon bar.

<img src="img/metamask.png" alt="metamask" width="30%">

If you click on it, Metamask will help you create an Ethereum account. Click on
_Get Started_, then _Create a Wallet_, then _No Thanks_. Create a password and click _Create_.
Then _Next_. A backup phrase will be generated. It can be used for later restoring your
Blockchain public/private keys. Click to reveal the phrase and store it somewhere safe as you'll need to
fill it in the next screen. Click _Next_. Fill in your backup phrase and click
_Confirm_. Click the _All Done_ button. Now you should see your Ethereum address and that you
own zero Ether. Your account is ready to use. Good job! You can pat yourself on the back.

To interact with Ethereum main network you will need Ether, Ethereum's crypto-currency,
which costs money. However,
for testing purposes, there are alternative Ethereum networks in which Ether is
worthless, thus free of charge. To make Metamask use such a network, at the top of
the Metamask screen click _Ethereum Mainnet_, then _Show/hide test networks_, then at _Show test networks_
select _On_. Now at the top of the Metamask screen instead of _Main Ethereum Network_
select _Ropsten Test Network_. Your Metamask should look like this:

<img src="img/zero_ether_metamask.png" alt="zero_ether_metamask" width="30%">

### Getting Ether
Now we need some Ether. We will use an existing smart contract in this Ethereum
network, called a _Test Faucet_, that owns Ether and can give us some
if we ask it nicely.

Copy your address from Metamask, go to the [faucet](https://faucet.egorfine.com/), paste it there and click
_Give me Ropsten ETH_. In around 1 minute you will see in Metamask that you 
have 0.3 Ether. In case you have problems with that faucet, try [this other faucet](https://fauceth.komputing.org/), or
[let us know and we'll send you some Ether](https://docs.google.com/document/d/1uNqfFBBYVl_CwDD8IjhLibhFEokfS8x1jBLJztcQLrU/edit?usp=sharing).

Good! We have Ether. Let's make a gift to someone.

### Transferring Ether
Let us now transfer some Ether to someone. Ask one of your colleagues to give you their
account address. You can use a chat or email. They can copy their address by
clicking on it in Metamask.

To transfer Ether to them, go to Metamask and click _Send_. At _Send to_ paste your
colleague's address. At _Amount_ type 0.001.

<img src="img/transfer.png" alt="transfer" width="30%">

Click _Next_ and then _Confirm_. Wait a few seconds for the transaction to get
confirmed. It should show up in Metamask. The exact same process can be used to
transfer Ether to a smart contract.

Your colleague should see now that their balance increased with 0.001 Ether.

Nice! We can transfer Ether. Let's see some smart contracts.

### Deploying a smart contract
We used a faucet to get some Ether. You might be wondering how does a faucet work.
So let us build a faucet ourselves. We prepared most of the code for a faucet smart 
contract for you. Let's deploy it to Ethereum, and later we shall explain how it works.

We will use Remix, an online IDE, to compile and deploy the contract. Go to https://remix.ethereum.org/. 
Click on the _Create New File_ button in the left top corner to add a new 
file (see below). Call it _Faucet.sol_. :warning: It's case-sensitive, so better don't misspell
the name!

<img src="img/new_contract.png" alt="new_contract" width="30%">

In the newly created tab copy-paste the code from this file: [Faucet.sol](Faucet.sol).
Let's compile it! Go to the _Solidity Compiler_ and click _Compile Faucet.sol_. If the compilation
succeeds, you should see no errors and your faucet is ready to deploy:

<img src="img/compilation_done.png" alt="compilation_done" width="30%">

To deploy the contract to Ethereum, go to the _Deploy & run transactions_ tab. We want to tell Remix
to deploy to the Ropsten Ethereum testnet. At _Environment_ select _Injected Web3_. Metamask
will open and ask for confirmation to connect to this website. Click _Next_ and then _Connect_.
You should now see _Ropsten network_ under the _Environment_ input field in Remix. Also, you see
that the Faucet is ready to deploy.

<img src="img/run.png" alt="run" width="30%">

Click on _Deploy_. Deploying a contract is an Ethereum transaction itself with the contract's
bytecode as payload. Metamask will
prompt you to confirm the transaction. As you can see the transaction has a gas fee,
which you have to always pay when transacting on Ethereum. Click _Confirm_. Wait a few
seconds for the transaction to get confirmed. Metamask will notify you. You see now
your deployed contract and its deployed address.

<img src="img/deployed.png" alt="deployed" width="30%">

:warning: If you ever need to redeploy a smart contract (e.g., because of a
mistake or you changed its source code) remove the old deployed contract from Remix by
pressing the _X_ button above.

Good job! You've got a smart contract deployed. Go brag!

### Understanding the Faucet contract
The way a faucet works is: people can transfer Ether to it, which it stores, and it
also provides a function that other people can use to withdraw Ether. As simple as that.
Let's now look at the source code of our Faucet contract. It is written in the Solidity
programming language. Solidity resembles very much object oriented programming, and a
contract is similar to an OOP class.

Our contract has three functions. Let's start with the last one:

```
    function() external payable { }
```

In Ethereum not only people can own Ether, but also **smart contracts can  own Ether**. In 
order for a
smart contract to be able to receive Ether it needs to have a function that is marked
_payable_. So this is all this function does. This function has no name.

The second function is:

```
    function faucetBalance() public view returns (uint256) {
        return address(this).balance;
    }
```

It can be used to get the current Ether balance of this smart contract. `this` keyword
represents the address of this smart contract. `address(...)` is basically a cast.
`.balance` is used for querying the Ether balance for an address: user or contract.

The last function is:
```
    function withdrawEther() public {
        // Insert your code here
    }
```

This is the function we will use to withdraw Ether from the faucet. You will have to
implement it later.

### Interacting with the Faucet contract
Let us first check the Ether balance of the contract. For that let's call the 
`faucetBalance()` function. The `view` classifier means that this is a "read-only" function.
It just reads the state of the contract. Since it's not changing the state of the contract,
**it does not result in an Ethereum transaction**. `withdrawEther()` on the other hand will change
the state of the contract, which means calling it **will result in an Ethereum transaction**.
In Remix go to your deployed contract and expand it by pressing the arrow
button. You will see all its functions there. Click on `faucetBalance`. The balance
should be zero.

<img src="img/balance.png" alt="balance" width="30%">

Later you will have to transfer Ether to it and the balance will increase.

Congratulations! You just learned to interact with your smart contract. Let's interact
with other people's contracts.

### The chocolate token
As we said earlier, we tokenized some physical chocolate and we deployed two smart
contracts on Ethereum so that you can get your chocolate from them. So in this case you
want to work with those specific contracts instead of deploying them yourself, because 
the ones you would deploy yourself would not have any chocolate.

The first smart contract is called _ChocolateToken.sol_. It oversees who owns 
how many chocolate tokens. You can see it as a bank, where 
people and smart contracts have chocolate token accounts (aka wallets). Let's load it in Remix. In Remix 
add one more file and call it
_ChocolateToken.sol_. :warning: Don't misspell the name! Into it copy-paste the code 
from this file:
[ChocolateToken.sol](ChocolateToken.sol). As you can see the code is just an interface containing the public functions the 
contract exposes, similar to Java interfaces and C/C++ headers. This is a very standard Ethereum
token, so we encourage you to check [its implementation](impl/ChocolateToken.sol).

Click _Compile ChocolateToken.sol_ in the _Solidity compiler_ tab.
Now instead of deploying the contract, you will have to load the contract we deployed
at this address: _0x63048fd3DCcE2b029b3C4774f67c7B718cBC8989_ . For this, go to the
_Deploy & run transactions_
tab, in the _At Address_ field fill in the address above, and
click on _At Address_. It should now show up in the _Deployed Contracts_.

<img src="img/loaded_chocolate.png" alt="loaded_chocolate" width="30%">

This contract has three functions for managing token ownership: `transfer()`,
`balanceOf()`, and `totalSupply()`.
These three functions are part of the ERC20 token standard, which many Ethereum clients, including
Metamask, support. This means that Metamask can show your wallet of chocolate tokens.
To do that, copy the contract's address by clicking on the copy icon (see above).

Then go to Metamask click on _Assets_, then _Import tokens_. In _Token Contract Address_ fill in the address
of the chocolate token contract from above.
In _Token Symbol_ fill in _ChocoCoins_. In _Token Decimal_ fill in _0_. Click _Add Custom Token_ 
and _Import Tokens_. You should now see that your
balance is zero. It will increase as the chocolate master starts handing you tokens.

### The chocolate master

The second contract we deployed is called _ChocolateMaster_. First add its code in Remix
from this source: [ChocolateMaster.sol](ChocolateMaster.sol), compile it, and load it
from this address: _0xEc74a15decC677844B7370d33C533C537d5Da601_ . You should see it
loaded.

<img src="img/loaded_chocolate_master.png" alt="loaded_chocolate_master" width="30%">

Awesome, the chocolate is up for grabs now! Go get it!

## Exercises

### 1. Basic income
Our chocolate master has been hanging out with his liberal friends and learned about the
idea of basic income, so why not try it out. The idea is that the
government pays every citizen a fixed basic income, independent of their
employment status, and the citizen can still choose to have a job to earn extra money.

So to get your basic income chocolate token, call the function `getBasicChocolateIncome()`
in the chocolate master contract. This changes the state of the `ChocolateToken` contract
since a token will move from the chocolate master's account to your account. So calling this
function will result in an Ethereum transaction. Metamask will ask you to sign this transaction.
After the transaction is accepted in Ethereum, you should see in Metamask that you received
a chocolate token.

### 2. Send Ether to your faucet

To make your faucet work you need to send it some Ether. Send it 0.00001 Ether. When you
are done, call `proveThatFaucetHasEther()` in the chocolate master contract, so that it can 
check that your faucet indeed has Ether. In the
function specify the address of your faucet contract. You should have two chocolate tokens now. 
You're doing very well!

### 3. Make your faucet withdrawable

Implement `withdrawEther()` such that it transfers some Ether to the account calling the function.
It does not matter how much Ether you transfer. Make sure though that your facet
has enough Ether. If it does not, send it more using Metamask as you did in the
previous task. :warning: Implement your method such that it withdraws much less than
1 Ether, so that your faucet does not run out of Ether.
Note that in Solidity 1 means 1 Wei, which is 1e(-18) Ether, so just make
your function withdraw 1 Wei. You can find a nice Ether/Wei converter here: http://eth-converter.com/.
When you are done, call `proveThatEtherCanBeWithdrawn()` in the chocolate master,
providing the address of your faucet contract. The master will try to withdraw Ether.
If it works, one more chocolate token for you.

:warning: If you change the code of your contract, don't forget to remove the old
deployed contract and recompile and redeploy your contract.

:warning: Don't forget to send Ether to your newly deployed faucet contract.

Hints:
* `msg.sender` is the address of the caller of the function.
* Solidity documentation: https://solidity.readthedocs.io/en/v0.5.2/units-and-global-variables.html#address-related

### Bonus: Make your faucet call the chocolate master

So far you have always called the chocolate master contract manually, but contracts can
call contracts, too. Add a function to your faucet that calls the function
`faucetInitiatesWithdraw()` from the chocolate master. The chocolate master will
try to withdraw Ether from your faucet so make sure your faucet has Ether.

:warning: Don't forget to send Ether to your newly deployed faucet contract.

Hint:
* when a contract calls another contract, in the code of the called contract 
`msg.sender` represents the address of the caller contract.

### Bonus: Check the chocolate master code

The chocolate master is a more complex smart contract, so feel free to check out 
[its implementation](impl/ChocolateMaster.sol).

## Chocolate time
Congratulations! You conquered the blockchain. You can come to us and retrieve your
delicious chocolate by showing us your token wallet. You deserved it! You can call
yourself a blockchain expert from now on.

Feel free to acquire some Ether and try the same exercise on Ethereum's main network.

