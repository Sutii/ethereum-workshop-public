pragma solidity ^0.4.24;

import './Faucet.sol';

contract ChocolateMaster {
    // Get the address of the chocolate token contract
    function getChocolateTokenContractAddr() public returns (address);

    // Call this to get a 'basic income' chocolate token
    function getBasicChocolateIncome() public;

    // Call this to prove that your faucet has ether and get a chocolate token
    function proveThatFaucetHasEther(Faucet faucet) public;

    // Call this to prove that ether can be withdrawn from your faucet and get one
    // chocolate token
    function proveThatEtherCanBeWithdrawn(Faucet faucet) public;

    // Bonus: Make your faucet contract itself call this method in order to make it
    // withdraw some ether.
    // The account has to be your account address. The chocolate token will be
    // sent to this address.
    function faucetInitiatesWithdraw(address accountToSendTheChocolateTokenTo) public;

    // This function makes it possible for the contract to receive ether
    function() external payable;
}
